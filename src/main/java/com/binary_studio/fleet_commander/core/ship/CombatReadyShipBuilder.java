package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShipBuilder {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public static CombatReadyShipBuilder named(String name) {
		var builder = new CombatReadyShipBuilder();

		builder.speed = PositiveInteger.of(1);
		builder.size = PositiveInteger.of(1);

		builder.name = name;

		return builder;
	}

	public CombatReadyShipBuilder pg(Integer val) {
		this.pg = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder hull(Integer val) {
		this.hullHP = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder shield(Integer val) {
		this.shieldHP = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder capacitorRegen(Integer val) {
		this.capacitorRegeneration = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder capacitor(Integer val) {
		this.capacitor = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder size(Integer size) {
		this.size = PositiveInteger.of(size);
		return this;
	}

	public CombatReadyShipBuilder speed(Integer speed) {
		this.size = PositiveInteger.of(speed);
		return this;
	}

	public CombatReadyShipBuilder attackSubsystem(AttackSubsystem attackSubsystem) {
		this.attackSubsystem = attackSubsystem;
		return this;
	}

	public CombatReadyShipBuilder defenciveSubsystem(DefenciveSubsystem defenciveSubsystem) {
		this.defenciveSubsystem = defenciveSubsystem;
		return this;
	}

	public CombatReadyShip construct() {
		return CombatReadyShip.construct(this.name, this.shieldHP, this.hullHP, this.pg, this.capacitor,
				this.capacitorRegeneration, this.speed, this.size, this.attackSubsystem, this.defenciveSubsystem);
	}

}
