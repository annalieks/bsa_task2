package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private int pgNeeded;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		DockedShip dockedShip = new DockedShip();
		dockedShip.name = name;
		dockedShip.shieldHP = shieldHP;
		dockedShip.hullHP = hullHP;
		dockedShip.pg = powergridOutput;
		dockedShip.capacitor = capacitorAmount;
		dockedShip.capacitorRegeneration = capacitorRechargeRate;
		dockedShip.speed = speed;
		dockedShip.size = size;
		return dockedShip;
	}

	private DockedShip() {
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.attackSubsystem = subsystem;
			return;
		}
		this.pgNeeded += subsystem.getPowerGridConsumption().value();
		if (this.pg.value() < this.pgNeeded) {
			throw new InsufficientPowergridException(this.pgNeeded - this.pg.value());
		}
		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = subsystem;
			return;
		}
		this.pgNeeded += subsystem.getPowerGridConsumption().value();
		if (this.pg.value() < this.pgNeeded) {
			throw new InsufficientPowergridException(this.pgNeeded - this.pg.value());
		}
		this.defenciveSubsystem = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return CombatReadyShipBuilder.named(this.name).shield(this.shieldHP.value()).hull(this.hullHP.value())
				.pg(this.pg.value()).capacitor(this.capacitor.value())
				.capacitorRegen(this.capacitorRegeneration.value()).speed(this.speed.value()).size(this.size.value())
				.attackSubsystem(this.attackSubsystem).defenciveSubsystem(this.defenciveSubsystem).construct();
	}

}
