package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private PositiveInteger initialCapacitor;

	private PositiveInteger initialHullHP;

	private PositiveInteger initialShieldHP;

	@Override
	public void endTurn() {
		int capacitorRecharged = this.capacitor.value() + this.capacitorRegeneration.value();
		if (capacitorRecharged > this.initialCapacitor.value()) {
			this.capacitor = this.initialCapacitor;
		}
		else {
			this.capacitor = PositiveInteger.of(capacitorRecharged);
		}
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.attackSubsystem.getCapacitorConsumption().value() > this.capacitor.value()) {
			return Optional.empty();
		}
		this.capacitor = PositiveInteger
				.of(this.capacitor.value() - this.attackSubsystem.getCapacitorConsumption().value());
		return Optional.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		attack = this.defenciveSubsystem.reduceDamage(attack);
		if (attack.damage.value() < this.shieldHP.value()) {
			this.shieldHP = PositiveInteger.of(this.shieldHP.value() - attack.damage.value());
			return new AttackResult.DamageRecived(attack.weapon, attack.damage, attack.target);
		}
		this.shieldHP = PositiveInteger.of(0);
		int damageForHull = attack.damage.value() - this.shieldHP.value();
		if (damageForHull > this.hullHP.value()) {
			return new AttackResult.Destroyed();
		}
		this.hullHP = PositiveInteger.of(this.hullHP.value() - damageForHull);
		return new AttackResult.DamageRecived(attack.weapon, attack.damage, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.defenciveSubsystem.getCapacitorConsumption().value() > this.capacitor.value()) {
			return Optional.empty();
		}
		this.capacitor = PositiveInteger
				.of(this.capacitor.value() - this.defenciveSubsystem.getCapacitorConsumption().value());

		RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
		int maxRegeneratedHullHP = regenerateAction.hullHPRegenerated.value();
		int maxRegeneratedShieldHP = regenerateAction.shieldHPRegenerated.value();
		int regeneratedHullHP = 0;
		int regeneratedShieldHP = 0;

		if (this.hullHP.value() + maxRegeneratedHullHP > this.initialHullHP.value()) {
			regeneratedHullHP = this.initialHullHP.value() - this.hullHP.value();
			if (this.shieldHP.value() + maxRegeneratedShieldHP > this.initialShieldHP.value()) {
				regeneratedShieldHP = this.initialShieldHP.value() - this.shieldHP.value();
				return Optional.of(new RegenerateAction(PositiveInteger.of(regeneratedShieldHP),
						PositiveInteger.of(regeneratedHullHP)));
			}
			return Optional.of(new RegenerateAction(PositiveInteger.of(maxRegeneratedShieldHP),
					PositiveInteger.of(regeneratedHullHP)));
		}
		if (this.shieldHP.value() + maxRegeneratedShieldHP > this.initialShieldHP.value()) {
			regeneratedShieldHP = this.initialShieldHP.value() - this.shieldHP.value();
			return Optional.of(new RegenerateAction(PositiveInteger.of(regeneratedShieldHP),
					PositiveInteger.of(maxRegeneratedHullHP)));
		}
		return Optional.of(regenerateAction);
	}

	public static CombatReadyShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		CombatReadyShip combatReadyShip = new CombatReadyShip();
		combatReadyShip.name = name;
		combatReadyShip.shieldHP = shieldHP;
		combatReadyShip.hullHP = hullHP;
		combatReadyShip.pg = powergridOutput;
		combatReadyShip.capacitor = capacitorAmount;
		combatReadyShip.capacitorRegeneration = capacitorRechargeRate;
		combatReadyShip.speed = speed;
		combatReadyShip.size = size;
		combatReadyShip.attackSubsystem = attackSubsystem;
		combatReadyShip.defenciveSubsystem = defenciveSubsystem;
		combatReadyShip.initialCapacitor = capacitorAmount;
		combatReadyShip.initialHullHP = hullHP;
		combatReadyShip.initialShieldHP = shieldHP;
		return combatReadyShip;
	}

}
