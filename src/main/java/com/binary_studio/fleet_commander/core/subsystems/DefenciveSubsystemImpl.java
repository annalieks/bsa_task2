package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger impactReduction;

	private PositiveInteger shieldRegen;

	private PositiveInteger hullRegen;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		DefenciveSubsystemImpl defenciveSubsystem = new DefenciveSubsystemImpl();
		defenciveSubsystem.name = name;
		defenciveSubsystem.pgRequirement = powergridConsumption;
		defenciveSubsystem.capacitorUsage = capacitorConsumption;
		defenciveSubsystem.impactReduction = impactReductionPercent;
		defenciveSubsystem.shieldRegen = shieldRegeneration;
		defenciveSubsystem.hullRegen = hullRegeneration;
		return defenciveSubsystem;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		int impactReductionPercent = this.impactReduction.value();
		if (impactReductionPercent > 95) {
			impactReductionPercent = 95;
		}
		int damage = (int) Math.ceil(incomingDamage.damage.value() * (100.0 - impactReductionPercent) / 100.0);
		if ((1.0 - (double) damage / incomingDamage.damage.value()) > 0.95) {
			damage = 95;
		}
		return new AttackAction(PositiveInteger.of(damage), incomingDamage.attacker, incomingDamage.target,
				incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

}
