package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger baseDamage;

	private PositiveInteger optimalSize;

	private PositiveInteger optimalSpeed;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		AttackSubsystemImpl attackSubsystem = new AttackSubsystemImpl();
		attackSubsystem.name = name;
		attackSubsystem.pgRequirement = powergridRequirments;
		attackSubsystem.capacitorUsage = capacitorConsumption;
		attackSubsystem.optimalSpeed = optimalSpeed;
		attackSubsystem.optimalSize = optimalSize;
		attackSubsystem.baseDamage = baseDamage;
		return attackSubsystem;
	}

	private AttackSubsystemImpl() {
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier;
		if (target.getSize().value() >= this.optimalSize.value()) {
			sizeReductionModifier = 1;
		}
		else {
			sizeReductionModifier = (double) target.getSize().value() / this.optimalSize.value();
		}
		double speedReductionModifier;
		if (target.getCurrentSpeed().value() <= this.optimalSpeed.value()) {
			speedReductionModifier = 1;
		}
		else {
			speedReductionModifier = (double) this.optimalSpeed.value() / (2 * target.getCurrentSpeed().value());
		}
		int damage = (int) Math
				.ceil((double) this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier));
		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {
		return this.name;
	}

}
