package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.Queue;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		Queue<Department> q = new LinkedList<>();
		q.add(rootDepartment);
		int maxDepth = 0;

		while (!q.isEmpty()) {
			maxDepth++;
			int queueSize = q.size();
			for (int i = 0; i < queueSize; i++) {
				Department tempDepartment = q.poll();
				assert tempDepartment != null;
				if (tempDepartment.subDepartments != null) {
					for (Department department : tempDepartment.subDepartments) {
						if (department != null) {
							q.add(department);
						}
					}
				}
			}
		}
		return maxDepth;
	}

}
